
# Getting Started with Deep Learning in ArcGIS Tutorial

Semantic Segmentation using UNET Framework}
In this project, we are using deep learning models to automatically extract the semantic class of pixels in satellite imagery. For an overview of how deep learning works, please ask me and we can schedule a zoom meeting. For an overview of how U-NET works, the specific model that we will be leveraging, feel free to contact me or see arcGIS post : https://developers.arcgis.com/python/guide/how-unet-works/

This tutorial will walk analysts through downloading all python package dependencies to use a pre-trained neural network to perform pixel-wise classification in ArcGIS. 

## Download and install Python dependencies

ArcGIS uses a very specific set of deep learning package versions. It is important to have the exact version of the required packages. To ensure this, I have a built a python package managing system that will create a new `conda` environment and assign it to the ArcGIS python interpreter. 
Software prerequisites include :

    1. Windows operating 
    2. ArcGIS Pro 2.3 or later
    3. ArcGIS Image Analyst license
    4. CPU or NVIDIA GPU  + CUDA CuDNN

## Getting started

1. Download .zip source code repository :  https://gitlab.com/maxfieldeland/dl_arcgis_tutorial
*Optional : if you currently have `git` version control set up on your machine, you may clone the repository, otherwise please use a direct download via the gitlab web page.
The repository contains a few different files. 
    
        a. azure_pixel_level_land_classification.emd ~ ESRI model definition text file
        b. example_RI_imagery.tif ~ 4-Band NAIP image tile
        c. 6_class_temperate.model ~ A pre-trained model architecture and weight set. This model pickle is defined by the Microsoft's Cognitive Toolkit (CNTK) deep learning framework.  
        env_setup.bat ~ a windows bat file that will install python dependencies specified in requirements.txt
        d. requirements.txt ~ list of core python package requirements. 
        e. Docs/ ~ supporting documents for projects, FAQ, writing emd files, writing raster functions. Images
2. Run env_setup.bat

        Running env_setup.bat  will install the required python packages into a new conda environment titled `DeepLearningArc`. 

        NOTE: By default, the batch installation will take up 5 GB of disk space on the C:/ drive (or the disk where ArcGIS Pro is installed). To execute the batch file, right click the batch file and choose to Run as Administrator. This installation takes less than 5 minutes to run on the author's test machine. This can take longer depending on machine and number of supporting dependencies required.

3. In the terminal window that opened to execute env_setup.bat run the following line of bash code `pip install cntk`. This will install the final package to run the deep learning programs!

4. Boot up ArcPro application. In Project Tab, navigate to Python and confirm that Project Environment is set too DeepLearningArc. You may need to restart ArcPro for the conda env changes to take place.

Once confirmed, you are ready to use ArcGIS Deep Learning tools! 

From here, you will be able to follow `Tutorial 2 : Image Labelling` or `Tutorial 3 : Pixel Classification` 