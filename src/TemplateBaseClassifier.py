'''
Copyright 2018 Esri

Licensed under the Apache License, Version 2.0 (the "License");

you may not use this file except in compliance with the License.

You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software

distributed under the License is distributed on an "AS IS" BASIS,

WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and

limitations under the License.​
'''

import json
import os
import sys
import keras 
from keras.models import load_model 
from models import unet_mse
import numpy as np

prf_root_dir = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.append(prf_root_dir)
import prf_utils

class TemplateBaseClassifier():

    def initialize(self, **kwargs):

        with open(kwargs['model'], 'r') as f:
            self.json_info = json.load(f)
       
        model_path = self.json_info['ModelFile']

        # load model, save model to model attr
        model = unet_mse(model_path,input_size = (32,32,4))
        self.model = model

    def getParameterInfo(self):

        return([
        {
            'name': 'raster',
            'dataType': 'raster',
            'required': True,
            'displayName': 'Raster',
            'description': 'Input Raster'
        },
        {
            'name': 'model',
            'dataType': 'string',
            'required': True,
            'displayName': 'Input Model Description (EMD) File',
            'description': 'Input model description (EMD) JSON file'
        }])
    def getConfiguration(self, **scalars):
        if 'BatchSize' not in self.json_info and 'batch_size' not in scalars:
            self.batch_size = 1
        elif 'BatchSize' not in self.json_info and 'batch_size' in scalars:
            self.batch_size = int(scalars['batch_size'])
        else:
            self.batch_size = int(self.json_info['BatchSize'])

        if 'ModelPadding' in self.json_info:
            self.model_padding = self.json_info['ModelPadding']
            self.padding = self.model_padding
        else:
            self.model_padding = 0
            self.padding = int(scalars['padding'])

        self.scalars = scalars

        self.rectangle_height, self.rectangle_width = prf_utils.calculate_rectangle_size_from_batch_size(self.batch_size)
        ty, tx = prf_utils.get_tile_size(self.json_info['ImageHeight'], self.json_info['ImageWidth'],
                                         self.padding, self.rectangle_height, self.rectangle_width)

        return {
            'extractBands': tuple(self.json_info['ExtractBands']),
            'padding': self.padding,
            'tx': tx,
            'ty': ty,
            'fixedTileSize': 1
        }


    def updateRasterInfo(self, **kwargs):
        kwargs['output_info']['bandCount'] = 1      # output is a single band raster
        return kwargs
        
    def updatePixels(self, tlc, shape, props, **pixelBlocks):
        input_image = pixelBlocks['raster_pixels']
        _, height, width = input_image.shape
        batch, batch_height, batch_width = \
            prf_utils.tile_to_batch(input_image,
                                    self.json_info['ImageHeight'],
                                    self.json_info['ImageWidth'],
                                    self.padding,
                                    fixed_tile_size=True,
                                    batch_height=self.rectangle_height,
                                    batch_width=self.rectangle_width)

     
        # reshape input from 1x4x32x32 to 
        reordered_array = np.zeros([batch.shape[2],batch.shape[3],batch.shape[1]]).reshape(1,32,32,4)
        for c in range(batch.shape[1]):
            reordered_array[0,:,:,c] = batch[0,c,:,:]
        array = reordered_array.copy()
        semantic_predictions = self.model.predict(array)

        semantic_predictions = prf_utils.batch_to_tile(semantic_predictions, batch_height, batch_width)

        # reshape predictions from 32x32x1 to 1x32x32

        reordered_array = np.zeros([32,32]).reshape([1,32,32])
        reordered_array[0,:,:] = semantic_predictions[:,:,0]
        array = reordered_array.copy()


        pixelBlocks['output_pixels'] = array.astype(props['pixelType'])

        return pixelBlocks
